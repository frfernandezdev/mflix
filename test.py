import math
import sys

class HandleErrors:
    def __init__(self):
        self._err = {}

    @property
    def error(self):
        return self._err

    @error.setter
    def error(self, value):
        if not tuple(value):
            raise ValueError("Value is not tuple")
        key, error = value
        self._err.setdefault(key, error)


class Utils:

    @staticmethod
    def isNone(value):
        return value is None

    @staticmethod
    def isNaN(value):
        return math.isnan(value)

    @staticmethod
    def isEmptyStr(value):
        return not bool(value and value.strip()) if isinstance(value, str) else False

    @staticmethod
    def isMatch(value, type):
        return isinstance(value, type)

    @classmethod
    def Is(cls, value, type):
        return False if (cls.isNone(value) or not cls.isMatch(value, type)) else True

    @staticmethod
    def isAny(value):
        return any(value)

    @staticmethod
    def isFunction(value):
        return callable(value)

    @classmethod
    def isDict(cls, value):
        return cls.Is(value, dict)

    @classmethod
    def isList(cls, value):
        return cls.Is(value, list)

    @classmethod
    def isExists(cls, value):
        return not cls.isNone(value) and not cls.isEmptyStr(value)

    @classmethod
    def typeMatch(cls, value, type):
        return cls.isAny(value, type) if cls.isList(type) else cls.Is(value, type)

    @staticmethod
    def OptionMatch(value, type):
        return (isinstance(value, tuple(type)))

    @classmethod
    def execute(cls, fn, value, key):
        result = fn(value, key)

        if result == value:
            if isinstance(value, dict):
                result = {}
                for x in value:
                    result[x] = cls.execute(fn, value[x], key)
                return result
            if isinstance(value, list):
                return map(lambda x: cls.execute(fn, x), value)
        return fn(value, key)

class Schema(Utils):
    def __init__(self, Schema):
        if isinstance(Schema, dict):
            self.schema = Schema
        else:
            raise ValueError("Schema is not dict")

        self.schema = self.loadSchema()

    def get_schema(self):
        return self.schema

    def loadSchema(self, schema=None):
        schema = schema or self.schema

        if self.isFunction(schema):
            schema = {"type": schema}
        elif self.isList(schema) and len(schema) > 1:
            schema = {"type": schema}
        elif self.isList(schema):
            schema = {"type": list, "items": schema[0]}
        elif self.isDict(schema) and not schema.get('type'):
            schema = {"type": dict, "properties": schema}

        if schema.get('type') is dict and schema.get('properties'):
            required = []
            for key in schema.get('properties'):
                if not isinstance(schema['properties'][key], Schema):
                    schema['properties'][key] = self.loadSchema(schema['properties'][key])

                if schema['properties'][key].get('default') is not None:
                    schema['default'] = schema.get('default') or {}
                    schema['default'][key] = schema['properties'][key].get('default')

                if schema['properties'][key].get('required'):
                    required.append(key)

            if len(required):
                schema['required'] = required if schema.get('required') is None else schema.get('required')

        if schema.get('type') is list:
            if not self.Is(schema.get('items'), Schema):
                schema['items'] = self.loadSchema(schema.get('items'))

        return schema

    def validate(self, data, options=None, schema=None):
        schema = schema or self.schema
        options = options or {}
        error = HandleErrors().error
        result = data.copy() if isinstance(data, dict) else data

        if not self.isExists(data):
            if not schema.get('required'):
                return True
            if self.isNone(data):
                raise ValueError("Required but passed None")
            if self.isEmptyStr(data):
                raise ValueError("Required but passed Empty String")
            raise ValueError("Required failure test for existence")

        if not self.isList(schema.get('type')):
            if not self.typeMatch(data, schema.get('type')):
                raise ValueError("Excepted {}".format(schema.get('type')))
        else:
            if not self.OptionMatch(data, schema.get('type')):
                raise ValueError("Excepted {}".format(schema.get('type')))

        if schema.get('validator') and not options.get('validator'):
            try:
                if not self.isList(schema.get('validator')):
                    schema.get('validator')(data)
                else:
                    for valid in schema.get('validator'):
                        valid(data)
            except (TypeError, ValueError) as e:
                error = "Validator", e

        if self.isDict(data):
            for key in data:
                try:
                    if schema['properties'].get(key):
                        result[key] = self.validate(data[key], options, schema['properties'][key])
                    else:
                        result.pop(key, None)
                except (TypeError, ValueError) as e:
                    error = key, e

            if schema.get('required') and not options.get('required'):
                for key in schema.get('required'):
                    try:
                        exists = self.isExists(data.get(key))
                        if not exists:
                            error = "Required " + key, ValueError("Required field {0}".format(key))
                    except (TypeError, ValueError) as e:
                        error = key, e

        if self.isList(data):
            for key, item in  enumerate(data):
                try:
                    result[key] = self.validate(item, options, schema.get('items'))
                except (TypeError, ValueError) as e:
                    error = key, e

        if schema.get('function'):
            result = schema.get('function')(data)

        if self.isDict(schema.get('default')):
            key = list(schema.get('default').keys())[0]
            if not data.get(key):
                values = list(schema.get('default').values())[0]
                result.setdefault(key, values)

        print(error)
        return result


class Base(Schema):
    def __init__(self, Schema):
        super(Base, self).__init__(Schema)

db = Base({
    "name": {
        "type": str,
        "required": True
    },
    "ci": [int, str],
    "gerens": int,
    "value": {
        "type": int,
        "default": 0
    },
    "array": [
        {
            "name": {
                "type": str,
                "default": "Fernando"
            }
        }
    ],
    "functions": {
        "type": int,
        "validator": lambda x: x,
        "function": lambda x: x*2
    }
})

response = db.validate({
    "name": "Fernando",
    "ci": 26290498,
    "gerens": 0,
    "value": 10,
    "array": [
        {
            "name": 0
        }
    ],
    "functions": 2,
    "lastname": "hello"
})

print(db.get_schema())
print(response)
